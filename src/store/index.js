import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex);

const API_URL = process.env.VUE_APP_API_URL;

export default new Vuex.Store({
    state: {
        user: JSON.parse(localStorage.getItem('user')) || null,
        diets: []
    },
    getters: {
        isLogged(state) {
            return state.user !== null;
        },
        user(state) {
            return state.user;
        },
        token(state) {
            return state.user ? state.user.token : null;
        },
    },
    mutations: {
        setUser(state, payload) {
            state.user = {
                email: payload.email,
                token: payload.access,
                refresh: payload.refresh
            };
            localStorage.setItem('user', JSON.stringify(state.user));
        },
        setAccessToken(state, payload) {
            console.log(payload)
            state.user = {
                ...state.user,
                token: payload
            };
            console.log(state.user)
            localStorage.setItem('user', JSON.stringify(state.user));
        },
        removeUser(state) {
            state.user = null;
            localStorage.removeItem('user');
        },

        setDiets(state, payload) {
            state.diets = payload;
        },
        removeDiet(state, payload) {
            state.diets = state.diets.filter((diet) => diet.id !== payload.id);
        },
        addDiet(state, payload) {
            state.diets = [payload, ...state.diets];
        },
    },
    actions: {
        login(context, payload) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'POST',
                    url: `${API_URL}/api/auth/token/`,
                    data: payload
                })
                    .then(function (response) {
                        if (response.status === 200) {
                            context.commit('setUser', {
                                ...response.data,
                                email: payload.email
                            });
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        },
        refreshToken(context, payload) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'POST',
                    url: `${API_URL}/api/auth/refresh/`,
                    data: {
                        refresh: payload
                    }
                })
                    .then(function (response) {
                        if (response.status === 200) {
                            context.commit('setAccessToken', response.data.access);
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        },
        logout(context) {
            context.commit('removeUser');
        },

        getDiets(context) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'GET',
                    url: `${API_URL}/api/diets/`
                })
                    .then(function (response) {
                        if (response.status === 200) {
                            context.commit('setDiets', response.data.results);
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        },
        removeDiet(context, payload) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'DELETE',
                    url: `${API_URL}/api/diets/${payload.id}/`
                })
                    .then(function (response) {
                        if (response.status === 204) {
                            context.commit('removeDiet', payload);
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            })
        },
        createDiet(context, payload) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'POST',
                    url: `${API_URL}/api/diets/`,
                    data: payload
                })
                    .then(function (response) {
                        if (response.status === 201) {
                            context.commit('addDiet', response.data);
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        },
        getDiet(context, payload) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'GET',
                    url: `${API_URL}/api/diets/${payload.id}/`,
                    data: payload
                })
                    .then(function (response) {
                        if (response.status === 200) {
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        },
        removeProductFromMeal(context, payload) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'DELETE',
                    url: `${API_URL}/api/meals/${payload.id}/remove-product`,
                    data: {product_id: payload.productId}
                })
                    .then(function (response) {
                        if (response.status === 204) {
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        },
        changeProductQuantityInMeal(context, payload) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'PATCH',
                    url: `${API_URL}/api/meals/${payload.mealId}/update-product-quantity/`,
                    data: {product_id: payload.productId, quantity: payload.quantity}
                })
                    .then(function (response) {
                        if (response.status === 200) {
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        },
        searchForProduct(context, payload) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'GET',
                    url: `${API_URL}/api/products/`,
                    params: {name: payload.name}
                })
                    .then(function (response) {
                        if (response.status === 200) {
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        },
        addProductToMeal(context, payload) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'POST',
                    url: `${API_URL}/api/meals/${payload.id}/add-product/`,
                    data: {product_id: payload.productId}
                })
                    .then(function (response) {
                        if (response.status === 201) {
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        },
        changeMealName(context, payload) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'PATCH',
                    url: `${API_URL}/api/meals/${payload.id}/`,
                    data: {name: payload.name}
                })
                    .then(function (response) {
                        if (response.status === 200) {
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        },
        deleteMeal(context, payload) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'DELETE',
                    url: `${API_URL}/api/meals/${payload.id}/`
                })
                    .then(function (response) {
                        if (response.status === 204) {
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            })
        },
        createMeal(context, payload) {
            return new Promise((resolve, reject) => {
                axios({
                    method: 'POST',
                    url: `${API_URL}/api/meals/`,
                    data: payload
                })
                    .then(function (response) {
                        if (response.status === 201) {
                            resolve(response);
                        }
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        },
    }
})
