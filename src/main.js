import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'

import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuelidate from 'vuelidate'

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.use(Vuelidate);
Vue.use(VueAxios, axios);
Vue.use(VueToast);
Vue.use(require('vue-moment'));

router.beforeEach((to, from, next) => {
    const isLogged = store.getters.isLogged;

    if (!isLogged && to.name === 'Login') {
        next();
    } else if (isLogged && to.name === 'Login') {
        next(from);
    } else {
        if (!isLogged) {
            next('/login');
        }
    }
    next();
});

// handle 401 and get new access token using refresh token
axios.interceptors.response.use(response => {
    return response;
}, async error => {
    if (error.response.status === 401) {
        const refreshToken = store.getters.user.refresh;
        await store.dispatch('refreshToken', refreshToken);
        router.go(0);
    }
    return error;
});

Vue.config.productionTip = false;

Vue.directive('focus', {
    inserted: function (el) {
        el.focus()
    }
});

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
