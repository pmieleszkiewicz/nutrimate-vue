import Vue from 'vue'
import VueRouter from 'vue-router'
import Diets from '../views/Diets.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: '/diets'
    },
    {
        path: '/diets',
        name: 'Diets',
        component: Diets
    },
    {
        path: '/products',
        name: 'Products',
        component: () => import(/* webpackChunkName: "about" */ '../views/Products.vue')
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
    },
    {
        path: '/diets/:id',
        name: 'DietDetail',
        component: () => import(/* webpackChunkName: "about" */ '../views/DietDetail.vue')
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
