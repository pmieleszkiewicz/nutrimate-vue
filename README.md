# Nutrimate (Nutrition App)

Vue.js which allows user to manage diets. Diet consists of meals, products are part of meals. 
It's a try to rewrite my [Laravel app](https://gitlab.com/pmieleszkiewicz/nutrimate) using Vue.js framework.

## Demo
Application is deployed on
[Heroku](https://nutrimate-vue.herokuapp.com/).

Django based API is avaiable [here](https://nutrimate-django.herokuapp.com/api/)

Login credentials:
```
Email: user@example.com
Password: secret_password
```


## Installation
Add .env.local file with variable with running Django Nutrimate app on port 8000:

```env
VUE_APP_API_URL=http://localhost:8000
```

```bash
npm install
npm run serve
```
